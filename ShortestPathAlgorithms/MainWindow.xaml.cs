﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Map;
using SearchSystem;
using Point = Map.Point;

namespace ShortestPathAlgorithms
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public int CountOfNodes { get; set; }
        public int CountOfBranches { get; set; }
        public int Seed { get; set; }
        public bool RandomWeight { get; set; }
        public Graph Graph { get; set; }
        public ObservableCollection<string> AlgorithmsList { get; set; }

        private string _headerText;
        public string HeaderText
        {
            get
            {
                return _headerText;
            }

            set
            {
                _headerText = value;
                OnPropertyChanged("HeaderText");
            }
        }

        private string _selectedAlg;
        public string SelectedAlg
        {
            get
            {
                return _selectedAlg;
            }

            set
            {
                _selectedAlg = value;
                OnPropertyChanged("SelectedAlg");
            }
        }

        public ISearchAlgorithm SearchAlgorithm { get; set; }

        #region Command

        public ICommand SearchCommand { get; set; }

        #endregion

        public MainWindow()
        {
            HeaderText = "SLACK";
            AlgorithmsList = new ObservableCollection<string>();
            AlgorithmsList.Add("Dijkstra");
            AlgorithmsList.Add("Bellman-Ford");
            AlgorithmsList.Add("A*");
            //AlgorithmsList.Add("IDA*");
            AlgorithmsList.Add("D*");
            AlgorithmsList.Add("Fringe");

            InitializeComponent();
            DataContext = this;
            SearchCommand = new DelegateCommand(SearchCommandOnExecute, SearchCommandCanExecute);

        }

        private void CountOfNodes_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox txtBox && !string.IsNullOrWhiteSpace(txtBox.Text))
            {
                CountOfNodes = int.Parse(txtBox.Text);
            }
        }
        private void CountOfBranches_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox txtBox && !string.IsNullOrWhiteSpace(txtBox.Text))
            {
                CountOfBranches = int.Parse(txtBox.Text);
            }
        }
        private void Seed_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox txtBox && !string.IsNullOrWhiteSpace(txtBox.Text))
            {
                Seed = int.Parse(txtBox.Text);
            }
        }
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void SearchCommandOnExecute(object sender)
        {
            if(Graph == null) return;
            ResetGraph();
            Graph.ShortestPath.Clear();
            HeaderText = SelectedAlg;
            switch(comboBox.SelectedIndex)
            {
                case 0:
                    SearchAlgorithm = new DijkstraSearch(Graph);
                    break;
                case 1:
                    SearchAlgorithm = new BellmanFord(Graph);
                    break;
                case 2:
                    SearchAlgorithm = new AStar(Graph);
                    break;
                case 3:
                    SearchAlgorithm = new IDAStar(Graph);
                    break;
                case 4:
                    SearchAlgorithm = new DStar(Graph);
                    break;
                case 5:
                    SearchAlgorithm = new FringeSearch(Graph);
                    break;
                default:
                    SearchAlgorithm = null;
                    break;
            }

            InvokeSearch();
        }

        private bool ValidateAllInputs()
        {
            var flag = true;
            if (CountOfNodes < 10 || CountOfNodes >= 1000000)
            {
                flag = false;
                MessageBox.Show("Invalid count of nodes! Please input number x: 10 < x < 1000 !");
            }

            if (CountOfBranches <= 1 || CountOfBranches > 1000000)
            {
                flag = false;
                MessageBox.Show("Invalid count of brunches! Please input number x: 1 < x < 10 !");
            }
            if (Seed < 1 || Seed >= 1000000)
            {
                flag = false;
                MessageBox.Show("Invalid seed! Please input number x: 1 < x < 100 !");
            }

            return flag;
        }

        private void PrintCanvas()
        {
            if(Graph == null) return;

            MainCanvas.Children.Clear();

            int xSide = (int)MainCanvas.ActualWidth;
            int ySide = (int)MainCanvas.ActualHeight;

            var widthOfNode = 6;

            foreach (var node in Graph.Nodes)
            {
                var x1 = (node.Point.X * xSide);
                var y1 = (node.Point.Y * ySide);

                var x = x1 - widthOfNode / 2;
                var y = y1 - widthOfNode / 2;

                var brush = Brushes.Gray;
                if (node.Visited)
                    brush = Brushes.Blue;
                if (node == Graph.StartNode)
                    brush = Brushes.GreenYellow;
                if (node == Graph.EndNode)
                    brush = Brushes.Red;

                var ellipse = new Ellipse { Width = widthOfNode, Height = widthOfNode };
                ellipse.Fill = brush;
                Canvas.SetLeft(ellipse, x);
                Canvas.SetTop(ellipse, y);

                ellipse.ToolTip = new ToolTip {Content = BuiltInfoString(node)};
                ellipse.MouseLeftButtonDown += EllipseOnMouseLeftButtonDown;
                CreateContextMenu(ellipse);
                MainCanvas.Children.Add(ellipse);

                foreach (var conn in node.Connections)
                {
                    var x2 = conn.ConnectedToNode.Point.X * xSide;
                    var y2 = conn.ConnectedToNode.Point.Y * ySide;

                    var line = new Line { X1 = x1, Y1 = y1, X2 = x2, Y2 = y2, Stroke = Brushes.Gray };
                    MainCanvas.Children.Add(line);
                }
            }
            for (int i = 0; i < Graph.ShortestPath.Count - 1; i++)
            {
                var firstNode = Graph.ShortestPath[i];
                var secondNode = Graph.ShortestPath[i + 1];

                var firstNodeX = firstNode.Point.X * xSide;
                var firstNodeY = firstNode.Point.Y * ySide;
                var secondNodeX = secondNode.Point.X * xSide;
                var secondNodeY = secondNode.Point.Y * ySide;

                var line = new Line()
                {
                    X1 = firstNodeX,
                    Y1 = firstNodeY,
                    X2 = secondNodeX,
                    Y2 = secondNodeY,
                    Stroke = Brushes.White,
                    StrokeThickness = 1.5
                };
                MainCanvas.Children.Add(line);
            }
        }

        private void EllipseOnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (sender != null && sender is Ellipse ellipse)
            {
                var x = Canvas.GetLeft(ellipse);
                var y = Canvas.GetTop(ellipse);
                int xSide = (int)MainCanvas.ActualWidth;
                int ySide = (int)MainCanvas.ActualHeight;

                var myX = (x + 3) / xSide;
                var myY = (y + 3) / ySide;

                Node node = null;

                foreach (var nod in Graph.Nodes)
                {
                    if (Math.Abs(nod.Point.X - myX) < 0.01 && Math.Abs(nod.Point.Y - myY) < 0.01)
                    {
                        node = nod;
                        break;
                    }
                }

                if (node != null)
                {
                    Graph.StartNode = node;
                }
            }
        }

        private string BuiltInfoString(Node node)
        {
            string result = "Name: " + node.Name;

            if (node.NearestToStart != null)
                result += "\n Nearest to start: " + node.NearestToStart.Name;

            if(node.MinCostToStart != null)
                result += "\n F: " + node.MinCostToStart;

            return result;

        }

        //private void AStarCommandOnExecute(object sender)
        //{
        //    if (Graph == null) return;
        //    ResetGraph();
        //    Graph.ShortestPath.Clear();
        //    HeaderText = "A *";
        //    SearchAlgorithm = new AStar(Graph);
        //    InvokeSearch();
        //}

        //private void BellmanFordCommandOnExecute(object sender)
        //{
        //    if (Graph == null) return;
        //    ResetGraph();
        //    Graph.ShortestPath.Clear();
        //    HeaderText = "Bellman-Ford";

        //    SearchAlgorithm = new BellmanFord(Graph);
        //    InvokeSearch();
        //}

        //private void FringeCommandOnExecute(object sender)
        //{
        //    if (Graph == null) return;
        //    ResetGraph();
        //    Graph.ShortestPath.Clear();
        //    HeaderText = "Fringe";

        //    SearchAlgorithm = new FringeSearch(Graph);
        //    InvokeSearch();

        //}

        //private void DStarCommandOnExecute(object sender)
        //{
        //    if (Graph == null) return;
        //    ResetGraph();
        //    Graph.ShortestPath.Clear();
        //    HeaderText = "D *";

        //    SearchAlgorithm = new DStar(Graph);
        //    InvokeSearch();
        //}

        private void IdaStarCommandOnExecute(object sender)
        {
            if (Graph == null) return;
            ResetGraph();
            Graph.ShortestPath.Clear();
            HeaderText = "IDA *";

            SearchAlgorithm = new IDAStar(Graph);
            InvokeSearch();
        }

        private void InvokeSearch()
        {
            if(SearchAlgorithm == null) return;
            var stopWatch = new Stopwatch();
            SearchAlgorithm.GetShortestPath(stopWatch);

            //PrintCanvas();
            var timeText = $"\nTime: {stopWatch.Elapsed.TotalMilliseconds}";

            resultTextBox.Text = "\t---RESULTS---" + 
                timeText + 
                "\nLength: " + SearchAlgorithm.LengthOfShortestPath + 
                "\nCost: " + SearchAlgorithm.CostOfShortestPath + 
                "\nNode visits: " + SearchAlgorithm.CountOfNodeVisit + 
                "\nNodes: " + Graph.Nodes.Count + 
                "\nEdges: " + Graph.NumberOfEdges;
        }

        private void BuildGraph_OnClick(object sender, RoutedEventArgs e)
        {
            if (!ValidateAllInputs()) return;
            Graph = Graph.GetRandomGraph(CountOfNodes, CountOfBranches, Seed, RandomWeight);
            //PrintCanvas();
            MessageBox.Show("GRAPH CREATED SUCCESSFULLY!", "INFORMATION", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void ResetGraph()
        {
            foreach (var node in Graph.Nodes)
            {
                node.MinCostToStart = null;
                node.Depth = 0;
                node.IsCompleted = false;
                node.Visited = false;
                node.Heuristics = 0;
                node.NearestToStart = null;
                node.Tmp = null;
                node.hMin = 0;
                node.parentIndex = 0;
                node.Tag = null;
            }
        }

        private void CreateContextMenu(Ellipse ellipse)
        {
            var contextMenu = new ContextMenu();
            var menuItem = new MenuItem();
            menuItem.Header = "Set start";
            menuItem.Click += SetStartPointClicked;
            contextMenu.Items.Add(menuItem);

            menuItem = new MenuItem();
            menuItem.Header = "Set end";
            menuItem.Click += SetEndPointClicked;
            contextMenu.Items.Add(menuItem);

            ellipse.ContextMenu = contextMenu;
        }

        private void SetEndPointClicked(object sender, RoutedEventArgs e)
        {
            var node = GetNodeFromMenuItem(sender as MenuItem);


            if (node != null)
            {
                Graph.EndNode = node;
            }
        }

        private void SetStartPointClicked(object sender, RoutedEventArgs e)
        {
            var node = GetNodeFromMenuItem(sender as MenuItem);

            if (node != null)
            {
                Graph.StartNode = node;
            }
        }

        private Node GetNodeFromMenuItem(MenuItem sender)
        {
            if (!(sender.Parent is ContextMenu parent)) return null;

            if (!(parent.PlacementTarget is Ellipse ellipse)) return null;

            var x = Canvas.GetLeft(ellipse);
            var y = Canvas.GetTop(ellipse);
            int xSide = (int)MainCanvas.ActualWidth;
            int ySide = (int)MainCanvas.ActualHeight;

            var myX = (x + 3) / xSide;
            var myY = (y + 3) / ySide;


            foreach (var nod in Graph.Nodes)
            {
                if (Math.Abs(nod.Point.X - myX) < 0.01 && Math.Abs(nod.Point.Y - myY) < 0.01)
                {
                    return nod;
                }
            }

            return null;
        }

        public bool SearchCommandCanExecute(object sender)
        {
            return Graph != null && comboBox.SelectedIndex != -1;
        }

        #region INotify
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion


    }
}
