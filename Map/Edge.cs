﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Map
{
    public class Edge
    {
        public double Length { get; set; }
        public double Cost { get; set; }
        public Node ConnectedToNode { get; set; }

        public override string ToString()
        {
            return "--->" + ConnectedToNode.ToString();
        }
    }
}
