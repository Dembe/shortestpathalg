﻿using System;
using System.Collections.Generic;

namespace Map
{
    public class Graph
    {
        public static Graph GetRandomGraph(int countOfNodes, int countOfBranches, int randomSeed, bool randomWeight)
        {
            var random = new Random(randomSeed);

            var graph = new Graph();

            for (int i = 0; i < countOfNodes; i++)
            {
                var node = Node.GenerateRandomNode(random, i.ToString());

                if(!node.IsCurrentNodeCloseToAny(graph.Nodes))
                    graph.Nodes.Add(node);
            }

            foreach (var node in graph.Nodes)
            {
                node.ConnectCurrentNodeToAnother(graph.Nodes, countOfBranches, random, randomWeight);
            }

            graph.GetNumberOfEdges();

            graph.EndNode = graph.Nodes[random.Next(graph.Nodes.Count - 1)];
            graph.StartNode = graph.Nodes[random.Next(graph.Nodes.Count - 1)];

            return graph;
        }

        public List<Node> Nodes { get; set; }

        public Node StartNode { get; set; }

        public Node EndNode { get; set; }

        public List<Node> ShortestPath { get; set; }

        public int NumberOfEdges { get; set; }

        public Graph()
        {
            Nodes = new List<Node>();
            ShortestPath = new List<Node>();
            NumberOfEdges = 0;
        }

        private void GetNumberOfEdges()
        {
            var temp = new List<Node>();

            foreach (var node in Nodes)
            {
                temp.Add(node);
                foreach (var connection in node.Connections)
                {
                    if (temp.IndexOf(connection.ConnectedToNode) == -1)
                    {
                        NumberOfEdges++;
                    }
                }
            }

        }
    }
}
