﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Map
{
    public class Node
    {
        public string Name { get; set; }
        public Point Point { get; set; }
        public List<Edge> Connections { get; set; } = new List<Edge>();
        public double? MinCostToStart { get; set; }
        public double? Tmp { get; set; }

        public Node NearestToStart { get; set; }
        public bool Visited { get; set; }
        public double Heuristics { get; set; }
        public int Depth { get; set; }

        // SMA Star
        public bool IsCompleted { get; set; }

        // D Star
        public string Tag { get; set; }
        public double hMin { get; set; }
        public int parentIndex { get; set; }

        public static Node GenerateRandomNode(Random random, string name)
        {
            return new Node
            {
                Point = new Point
                {
                    X = random.NextDouble(),
                    Y = random.NextDouble()
                },
                Name = name
            };
        }

        public double GetDistanceFromCurrentTo(Node node)
        {
            return Math.Sqrt(Math.Pow(Point.X - node.Point.X, 2) + Math.Pow(Point.Y - node.Point.Y, 2));
        }

        public bool IsCurrentNodeCloseToAny(List<Node> nodes)
        {
            foreach (var node in nodes)
            {
                if (GetDistanceFromCurrentTo(node) < 0.001)
                    return true;
            }

            return false;
        }

        public void ConnectCurrentNodeToAnother(List<Node> nodes, int countOfBranches, Random random, bool randomWeight)
        {
            var allConnections = new List<Edge>();
            int numberOfEdges = 0;
            foreach (var node in nodes)
            {
                if(Name == node.Name)
                    continue;

                var distance = GetDistanceFromCurrentTo(node);

                allConnections.Add(new Edge
                {
                    ConnectedToNode = node,
                    Length = distance,
                    Cost = randomWeight ? random.NextDouble() : distance
                });
            }
            allConnections = allConnections.OrderBy(x => x.Length).ToList();
            var counter = 0;

            foreach (var connection in allConnections)
            {
                // Connect close node.
                if(Connections.All(conn => conn.ConnectedToNode != connection.ConnectedToNode))
                    Connections.Add(connection);
                counter++;

                // Connect another node to Edge if not already connected
                if (connection.ConnectedToNode.Connections.All(con => con.ConnectedToNode != this))
                {
                    numberOfEdges++;
                    var back = new Edge
                    {
                        ConnectedToNode = this,
                        Length = connection.Length,
                        Cost = connection.Cost
                    };
                    connection.ConnectedToNode.Connections.Add(back);
                }

                if(counter == countOfBranches)
                    return;
            }
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public class Point
    {
        public double X { get; set; }
        public double Y { get; set; }
    }
}
