﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Map;

namespace SearchSystem
{
    public class SMAStar : ISearchAlgorithm
    {
        public int CountOfNodeVisit { get; set; }
        public double LengthOfShortestPath { get; set; }
        public double CostOfShortestPath { get; set; }
        public Node Start { get; set; }
        public Node End { get; set; }
        public Graph Graph { get; set; }
        public int MaxDepth { get; set; }
        public int MaxSizeOfList { get; set; }
        public int ListCounter { get; set; }

        public SMAStar(Graph graph, int maxDepth, int maxSizeOfList)
        {
            Graph = graph;
            Start = Graph.StartNode;
            End = Graph.EndNode;
            MaxDepth = maxDepth;
            MaxSizeOfList = maxSizeOfList;
        }

        public void GetShortestPath(Stopwatch stopWatch)
        {
            //Search();
            var priorityQueue = new List<Node>();

            stopWatch.Start();
            CountOfNodeVisit = 0;
            priorityQueue.Add(Start);
            ListCounter = 1;
            Start.MinCostToStart = 0;
            Start.hMin = 0;
            Start.Depth = 0;
            while (true)
            {
                if (priorityQueue.Count < 1)
                    return;
                //priorityQueue = priorityQueue.OrderBy(x => x.MinCostToStart + x.Heuristics).ToList();

                var node = RemoveNode(priorityQueue);
                CountOfNodeVisit++;
                if (node.Name == "20")
                {
                    var a = 1;
                }
                if (node == End)
                {
                    BuiltRecursiveShortestPath(Graph.ShortestPath, node);
                    Graph.ShortestPath.Reverse();
                    stopWatch.Stop();

                    return;
                }
                else
                {
                    foreach (var connection in node.Connections)
                    {
                        var childNode = connection.ConnectedToNode;

                        double? res = 0;
                        //childNode.Visited = false;
                        if (childNode != End && childNode.Depth == MaxDepth)
                            childNode.MinCostToStart = double.MaxValue;
                        else
                        {
                            res = childNode.MinCostToStart;
                            //childNode.MinCostToStart = childNode.Tmp + GetHeuristics(childNode, End);
                        }

                        if (childNode.Visited == true && res != null && childNode.MinCostToStart >
                            node.hMin + connection.Cost + GetHeuristics(childNode, End))
                            childNode.Visited = false;

                        if (childNode.Visited)
                            continue;

                        if (res == null || childNode.MinCostToStart > node.hMin + connection.Cost + GetHeuristics(childNode, End))
                        {
                            childNode.NearestToStart = node;
                            childNode.hMin = node.hMin + connection.Cost;
                            childNode.MinCostToStart = GetMax((double)node.MinCostToStart, (double)childNode.hMin + GetHeuristics(childNode, End));
                        }
                        childNode.Depth = node.Depth + 1;

                        if (!InsertNode(priorityQueue, childNode))
                        {
                            Node badNode = priorityQueue[priorityQueue.Count - 1];
                            priorityQueue.Remove(badNode);
                            //badNode.Visited = false;
                        }
                    }

                    node.Visited = true;
                }
            }

        }

        public void Search()
        {
            var priorityQueue = new List<Node>();


            priorityQueue.Add(Start);
            ListCounter = 1;
            Start.MinCostToStart = 0;
            Start.Depth = 0;

            int countOfConnections;
            int connectionIterrator = 0;
            int USED = 1;
            Node oldNode = null;
            while (true)
            {
                if (priorityQueue.Count < 1)
                    return;
                //priorityQueue = priorityQueue.OrderBy(x => x.MinCostToStart + x.Heuristics).ToList();
                
                var node = Remove1(priorityQueue);
                if (node != oldNode)
                    connectionIterrator = 0;
                if (node == End)
                {
                    BuiltRecursiveShortestPath(Graph.ShortestPath, node);
                    Graph.ShortestPath.Reverse();

                    return;
                }
                else
                {
                    countOfConnections = node.Connections.Count;
                    var child = node.Connections[connectionIterrator].ConnectedToNode;
                    if (node.NearestToStart == child)
                    {
                        oldNode = node;
                        connectionIterrator++;
                        continue;
                    }
                    if (child.Visited)
                        continue;
                    child.NearestToStart = node;
                    child.Depth = node.Depth + 1;
                    child.MinCostToStart = GetMax((double)node.MinCostToStart,
                        (double)(node.MinCostToStart) + node.Connections[connectionIterrator].Cost + GetHeuristics(child, End));

                    if (connectionIterrator == countOfConnections - 1)
                    {
                        node.IsCompleted = true;
                        BackUp(node);
                    }
                    var listOfChilds = new List<Node>();
                    foreach (var conn in node.Connections)
                    {
                        listOfChilds.Add(conn.ConnectedToNode);
                    }

                    var memoryList = new List<Node>();
                    foreach (var pNode in priorityQueue)
                    {
                        memoryList.Add(pNode);
                    }
                    memoryList.Add(child);
                    var wasRemoved = false;
                    if (CheckList(memoryList, listOfChilds))
                    {
                        priorityQueue.Remove(node);
                        wasRemoved = true;
                    }

                    USED += 1;

                    if (USED > MaxSizeOfList)
                    {
                        var removed = Remove2(priorityQueue);
                        removed.Visited = true;
                        priorityQueue.Remove(removed);
                        if(removed.NearestToStart != null)
                            priorityQueue.Add(removed.NearestToStart);
                        USED--;
                    }
                    priorityQueue.Add(child);
                    connectionIterrator++;
                    oldNode = node;
                    if (wasRemoved)
                        connectionIterrator = 0;
                }
            }
        }

        private void BackUp(Node node)
        {
            if (node.IsCompleted && node.NearestToStart != null)
            {
                var oldValue = node.MinCostToStart;
                node.MinCostToStart = LeastFCostChilds(node);

                if (oldValue != node.MinCostToStart)
                {
                    BackUp(node.NearestToStart);
                }
            }
        }

        private double LeastFCostChilds(Node node)
        {
            var result = node.Connections[0].ConnectedToNode.MinCostToStart;
            foreach (var conn in node.Connections)
            {
                if (conn.ConnectedToNode.MinCostToStart < result)
                    result = conn.ConnectedToNode.MinCostToStart;
            }

            return (double)result;
        }

        private bool CheckList(List<Node> list, List<Node> childs)
        {
            foreach (var node in childs)
            {
                if (list.FirstOrDefault(x => x == node) == null)
                    return false;
            }

            return true;
        }
        public void BuiltRecursiveShortestPath(List<Node> list, Node node)
        {
            while (node.NearestToStart != null)
            {
                // SUCCESS
                list.Add(node);
                LengthOfShortestPath += node.Connections.Single(x => x.ConnectedToNode == node.NearestToStart).Length;
                CostOfShortestPath += node.Connections.Single(x => x.ConnectedToNode == node.NearestToStart).Cost;

                node = node.NearestToStart;
            }
            list.Add(node);
        }

        private double GetMax(double a, double b)
        {
            return a > b ? a : b;
        }

        private double GetHeuristics(Node firstNode, Node secondNode)
        {
            return Math.Sqrt(Math.Pow(firstNode.Point.X - secondNode.Point.X, 2) + Math.Pow(firstNode.Point.Y - secondNode.Point.Y, 2));
        }

        private void UpdateAncestors(Node node, double cost)
        {
            if(node == null) return;
            node.MinCostToStart = cost;
            UpdateAncestors(node.NearestToStart, cost);
        }

        private Node RemoveNode(List<Node> list)
        {
            int index = 0;
            var root = list[index];
            list[index] = list[--ListCounter];
            int indexOfLargestChild;

            Node top = list[index];

            while (index < ListCounter / 2)
            {
                int indexOfLeftChild = 2 * index + 1;
                int indexOfRightChild = indexOfLeftChild + 1;

                if (indexOfRightChild < ListCounter &&
                    list[indexOfLeftChild].MinCostToStart > list[indexOfRightChild].MinCostToStart)
                    indexOfLargestChild = indexOfRightChild;
                else   
                    indexOfLargestChild = indexOfLeftChild;

                if(top.MinCostToStart < list[indexOfLargestChild].MinCostToStart)
                    break;

                list[index] = list[indexOfLargestChild];
                index = indexOfLargestChild;
            }

            list[index] = top;

            return root;
        }

        private Node Remove1(List<Node> list)
        {
            int depth = 0;
            foreach (var node in list)
            {
                if (node.Depth > depth)
                    depth = node.Depth;
            }

            var removeNode = list.FirstOrDefault(x => x.Depth == depth);

            foreach (var node in list)
            {
                if (node.Depth == depth && node.MinCostToStart < removeNode.MinCostToStart)
                    removeNode = node;
            }

            return removeNode;
        }

        private Node Remove2(List<Node> list)
        {
            int depth = list[0].Depth;
            foreach (var node in list)
            {
                if (node.Depth < depth)
                    depth = node.Depth;
            }

            var removeNode = list.FirstOrDefault(x => x.Depth == depth);

            foreach (var node in list)
            {
                if (node.Depth == depth && node.MinCostToStart > removeNode.MinCostToStart)
                    removeNode = node;
            }

            return removeNode;
        }

        private bool InsertNode(List<Node> list, Node node)
        {
            if (ListCounter == MaxSizeOfList) return false;

            if(list.Count == ListCounter)
                list.Add(node);
            else
              list[ListCounter] = node;
            
            ListCounter++;
            var index = list.Count - 1;
            int parent = (index - 1) / 2;
            Node bottomNode = list[index];

            while (index > 0 && list[parent].MinCostToStart > bottomNode.MinCostToStart)
            {
                list[index] = list[parent];
                index = parent;
                parent = (parent - 1) / 2;
            }

            list[index] = bottomNode;

            return true;
        }
    }
}
