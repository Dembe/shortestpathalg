﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using Map;

namespace SearchSystem
{
    public class FringeSearch : ISearchAlgorithm
    {
        public int CountOfNodeVisit { get; set; }
        public double LengthOfShortestPath { get; set; }
        public double CostOfShortestPath { get; set; }
        public Node Start { get; set; }
        public Node End { get; set; }
        public Graph Graph { get; set; }

        public Dictionary<Node, Cache> CacheDictionary { get; set; }

        public FringeSearch(Graph graph)
        {
            Graph = graph;
            Start = graph.StartNode;
            End = graph.EndNode;
            LengthOfShortestPath = 0;
            CountOfNodeVisit = 0;
            CountOfNodeVisit = 0;
            CacheDictionary = new Dictionary<Node, Cache>();
        }

        public void GetShortestPath(Stopwatch stopWatch)
        {
            var priorityQueue = new List<Node>();
            priorityQueue.Add(Start);
            CacheDictionary.Add(Start, new Cache(0, null));

            double fLimit = GetHeuristics(Start, End);
            bool found = false;
            stopWatch.Start();
            while (!found && priorityQueue.Count > 0)
            {
                double fMin = double.MaxValue;

                int countPQ = priorityQueue.Count;
                int counter = 0;
                for (int i = 0; i < countPQ; i++)
                {
                    //priorityQueue = priorityQueue.OrderBy(x => x.MinCostToStart).ToList();
                    var node = priorityQueue[counter];
                    node.Visited = true;
                    var currG = CacheDictionary[node].GCost;
                    var parent = CacheDictionary[node].ParentNode;
                    CountOfNodeVisit++;
                    var fCost = currG + GetHeuristics(node, End);

                    if (fCost > fLimit)
                    {
                        fMin = GetMin(fCost, fMin);
                        counter = i + 1;
                        continue;
                    }

                    if (node == End)
                    {
                        found = true;
                        break;
                    }

                    //var childrenList = GetChildren(node);
                    foreach (var connection in node.Connections)
                    {
                        if (connection.ConnectedToNode.Visited)
                            continue;
                        var gChild = currG + connection.Cost;
                        if (CacheDictionary.ContainsKey(connection.ConnectedToNode))
                        {
                            if (gChild >= CacheDictionary[connection.ConnectedToNode].GCost)
                                continue;
                        }

                        if (priorityQueue.IndexOf(connection.ConnectedToNode) != -1)
                        {
                            priorityQueue.Remove(connection.ConnectedToNode);
                        }

                        priorityQueue.Add(connection.ConnectedToNode);
                        if (CacheDictionary.ContainsKey(connection.ConnectedToNode))
                        {
                            CacheDictionary[connection.ConnectedToNode].GCost = gChild;
                            CacheDictionary[connection.ConnectedToNode].ParentNode = node;
                        }
                        else
                        {
                            CacheDictionary.Add(connection.ConnectedToNode, new Cache(gChild, node));
                        }

                        connection.ConnectedToNode.MinCostToStart = gChild;
                    }

                    counter = 0;
                    priorityQueue.Remove(node);
                    //
                    //countPQ = priorityQueue.Count;
                }

                fLimit = fMin;
            }

            if (found)
            {
                Graph.ShortestPath.Add(End);
                BuiltRecursiveShortestPath(priorityQueue, End);
                Graph.ShortestPath.Reverse();
            }
            stopWatch.Stop();
        }


        //public void GetShortestPath(Stopwatch stopWatch)
        //{
        //    var priorityQueue = new Dictionary<int, Node>();
        //    priorityQueue.Add(0, Start);
        //    Start.MinCostToStart = 0;
        //    double fLimit = GetHeuristics(Start, End);
        //    bool found = false;
        //    stopWatch.Start();
        //    while (!found && priorityQueue.Count > 0)
        //    {
        //        double fMin = double.MaxValue;

        //        int countPQ = priorityQueue.Count;
        //        int counter = 0;
        //        for (int i = 0; i < countPQ; i++)
        //        {
        //            priorityQueue.OrderBy(x => x.Value.MinCostToStart);
        //            var node = priorityQueue[counter];
        //            node.Visited = true;
        //            var currG = node.MinCostToStart;
        //            CountOfNodeVisit++;
        //            var fCost = currG + GetHeuristics(node, End);

        //            if (fCost > fLimit)
        //            {
        //                fMin = GetMin((double)fCost, fMin);
        //                counter = i + 1;
        //                continue;
        //            }

        //            if (node == End)
        //            {
        //                found = true;
        //                break;
        //            }

        //            var childrenList = GetChildren(node);
        //            foreach (var connection in childrenList)
        //            {
        //                var gChild = currG + connection.Cost;
        //                if (connection.ConnectedToNode.MinCostToStart != null)
        //                {
        //                    if (gChild >= connection.ConnectedToNode.MinCostToStart)
        //                        continue;
        //                }

        //                if (priorityQueue.ContainsValue(connection.ConnectedToNode))
        //                {
        //                    priorityQueue.Remove(priorityQueue.FirstOrDefault(x => x.Value == connection.ConnectedToNode).Key);
        //                }

        //                priorityQueue.Add(priorityQueue.Count, connection.ConnectedToNode);
        //                connection.ConnectedToNode.MinCostToStart = gChild;
        //                connection.ConnectedToNode.NearestToStart = node;
        //            }

        //            counter = 0;
        //            priorityQueue.Remove(priorityQueue.FirstOrDefault(x => x.Value == node).Key);
        //        }

        //        fLimit = fMin;
        //    }

        //    if (found)
        //    {
        //        Graph.ShortestPath.Add(End);
        //        BuiltRecursiveShortestPath(Graph.ShortestPath, End);
        //        Graph.ShortestPath.Reverse();
        //    }
        //    stopWatch.Stop();
        //}

        public void BuiltRecursiveShortestPath(List<Node> list, Node node)
        {
            var parent = CacheDictionary[node].ParentNode;
            if (parent != null)
            {
                Graph.ShortestPath.Add(parent);
                LengthOfShortestPath += node.Connections.Single(x => x.ConnectedToNode == parent).Length;
                CostOfShortestPath += node.Connections.Single(x => x.ConnectedToNode == parent).Cost;
                BuiltRecursiveShortestPath(list, parent);
            }
        }

        //public void BuiltRecursiveShortestPath(List<Node> list, Node node)
        //{
        //    if (node.NearestToStart == null) return;

        //    list.Add(node.NearestToStart);
        //    LengthOfShortestPath += node.Connections.Single(x => x.ConnectedToNode == node.NearestToStart).Length;
        //    CostOfShortestPath += node.Connections.Single(x => x.ConnectedToNode == node.NearestToStart).Cost;
        //    BuiltRecursiveShortestPath(list, node.NearestToStart);
        //}

        private List<Edge> GetChildren(Node node)
        {
            var result = new List<Edge>();

            foreach (var c in node.Connections)
            {
                if(c.ConnectedToNode.Visited)
                    continue;
                result.Add(c);
            }

            result = result.OrderBy(x => x.ConnectedToNode.MinCostToStart).ToList();
            result.Reverse();
            return result;
        }

        public double GetHeuristics(Node firstNode, Node secondNode)
        {
            return Math.Sqrt(Math.Pow(firstNode.Point.X - secondNode.Point.X, 2) +
                             Math.Pow(firstNode.Point.Y - secondNode.Point.Y, 2));
        }

        public double GetMin(double a, double b)
        {
            return a < b ? a : b;
        }

        public class Cache
        {
            public double GCost { get; set; }
            public Node ParentNode { get; set; }

            public Cache(double gCost, Node parentNode)
            {
                GCost = gCost;
                ParentNode = parentNode;
            }
        }
    }
}