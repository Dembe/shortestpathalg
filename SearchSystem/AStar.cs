﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Map;

namespace SearchSystem
{
    public class AStar : ISearchAlgorithm
    {
        public int CountOfNodeVisit { get; set; }
        public double LengthOfShortestPath { get; set; }
        public double CostOfShortestPath { get; set; }
        public Node Start { get; set; }
        public Node End { get; set; }
        public Graph Graph { get; set; }

        public AStar(Graph graph)
        {
            Graph = graph;
            End = graph.EndNode;
            Start = graph.StartNode;
        }

        public void GetShortestPath(Stopwatch stopWatch)
        {      
            CountOfNodeVisit = 0;
            Start.MinCostToStart = 0;

            stopWatch.Start();

            var priorityQueue = new List<Node>();
            priorityQueue.Add(Start);
            do
            {
                priorityQueue = priorityQueue.OrderBy(x => x.MinCostToStart + GetHeuristics(x, End)).ToList();
                var currentNode = priorityQueue.First();

                priorityQueue.Remove(currentNode);
                CountOfNodeVisit++;

                foreach (var conn in currentNode.Connections.OrderBy(x => x.Cost))
                {
                    var child = conn.ConnectedToNode;

                    if (child.Visited) continue;

                    if (child.MinCostToStart == null || currentNode.MinCostToStart + conn.Cost < child.MinCostToStart)
                    {
                        child.MinCostToStart = currentNode.MinCostToStart + conn.Cost;
                        child.NearestToStart = currentNode;
                        if (!priorityQueue.Contains(child))
                            priorityQueue.Add(child);
                    }
                }

                currentNode.Visited = true;
                if (currentNode == End) break;
            } while (priorityQueue.Any());

            Graph.ShortestPath.Add(End);
            BuiltRecursiveShortestPath(Graph.ShortestPath, End);
            Graph.ShortestPath.Reverse();

            stopWatch.Stop();
        }

        public void BuiltRecursiveShortestPath(List<Node> list, Node node)
        {
            if (node.NearestToStart == null) return;

            list.Add(node.NearestToStart);
            LengthOfShortestPath += node.Connections.Single(x => x.ConnectedToNode == node.NearestToStart).Length;
            CostOfShortestPath += node.Connections.Single(x => x.ConnectedToNode == node.NearestToStart).Cost;
            BuiltRecursiveShortestPath(list, node.NearestToStart);
        }

        public double GetHeuristics(Node firstNode, Node secondNode)
        {
            //return Math.Abs(firstNode.Point.X - secondNode.Point.X) + Math.Abs(firstNode.Point.Y - secondNode.Point.Y);
            return Math.Sqrt(Math.Pow(firstNode.Point.X - secondNode.Point.X, 2) + Math.Pow(firstNode.Point.Y - secondNode.Point.Y, 2));
        }
    }
}
