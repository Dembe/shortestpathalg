﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Map;

namespace SearchSystem
{
    public class IDAStar : ISearchAlgorithm
    {
        public int CountOfNodeVisit { get; set; }
        public double LengthOfShortestPath { get; set; }
        public double CostOfShortestPath { get; set; }
        public Node Start { get; set; }
        public Node End { get; set; }
        public Graph Graph { get; set; }
        public List<Node> VisitedNodes { get; set; }

        public IDAStar(Graph graph)
        {
            Graph = graph;
            End = graph.EndNode;
            Start = graph.StartNode;
            VisitedNodes = new List<Node>();
        }
        public void GetShortestPath(Stopwatch stopWatch)
        {
            double threshold = GetHeuristics(Start, End);
            CountOfNodeVisit = 0;
            Start.MinCostToStart = 0;
            stopWatch.Start();
            var priopityQueue = new Dictionary<string, Node>();
            priopityQueue.Add(Start.Name, Start);
            while (true)
            {
                double tmp = Search2(priopityQueue, 0, threshold);
                if (tmp == -1)
                {
                    Start.NearestToStart = null;
                    BuiltRecursiveShortestPath(Graph.ShortestPath, End);
                    stopWatch.Stop();
                    return;
                }

                if (tmp == double.MaxValue)
                    return;
                threshold = tmp;
            }
        }

        private double Search(Node node, double gValue, double threshold)
        {
            double cost = gValue + GetHeuristics(node, End);
            if (node.Visited)
                return double.MaxValue;
            if (cost > threshold)
                return cost;
            if (node == End)
                return -1;
            double min = double.MaxValue;
            foreach (var connection in node.Connections)
            {
                node.Visited = true;
                double temp = Search(connection.ConnectedToNode, gValue + connection.Cost, threshold);
                if (temp == -1)
                {
                    connection.ConnectedToNode.NearestToStart = node;
                    return -1;
                }
                if (temp < min)
                {
                    min = temp;
                    connection.ConnectedToNode.NearestToStart = node;
                }
            }

            return min;

        }

        //private double Search2(List<Node> path, double gValue, double threshold)
        //{
        //    var node = path[path.Count - 1];

        //    node.MinCostToStart = gValue + GetHeuristics(node, End);
        //    if (node.MinCostToStart > threshold)
        //        return (double)node.MinCostToStart;
        //    if (node == End)
        //        return -1;
        //    double min = double.MaxValue;
        //    CountOfNodeVisit++;
        //    node.Visited = true;
        //    VisitedNodes.Add(node);
        //    foreach (var connection in node.Connections)
        //    {
        //        if ((path.IndexOf(connection.ConnectedToNode) != -1) || connection.ConnectedToNode.Visited)
        //            continue;

        //        path.Add(connection.ConnectedToNode);
        //        double temp = Search2(path, gValue + connection.Cost, threshold);
        //        if (temp == -1)
        //        {
        //            connection.ConnectedToNode.NearestToStart = node;

        //            return -1;
        //        }
        //        if (temp < min)
        //        {
        //            connection.ConnectedToNode.NearestToStart = node;

        //            min = temp;
        //        }

        //        path.Remove(connection.ConnectedToNode);
        //    }

        //    return min;

        //}

        private double Search2(Dictionary<string, Node> path, double gValue, double threshold)
        {
            var node = path.Values.Last();
            double cost = gValue + GetHeuristics(node, End);
            if (cost > threshold)
            {
                node.MinCostToStart = cost;
                return cost;
            }

            if (node == End)
            {
                return -1;
            }
            double min = double.MaxValue;
            CountOfNodeVisit++;
            foreach (var connection in node.Connections)
            {
                if ((path.ContainsValue(connection.ConnectedToNode)))
                    continue;

                path.Add(connection.ConnectedToNode.Name, connection.ConnectedToNode);
                double temp = Search2(path, gValue + connection.Cost, threshold);
                if (temp == -1)
                {
                    connection.ConnectedToNode.NearestToStart = node;
                    return -1;
                }
                if (temp < min)
                {
                    min = temp;
                }

                path.Remove(connection.ConnectedToNode.Name);
            }
            return min;
        }

        public void BuiltRecursiveShortestPath(List<Node> list, Node node)
        {
            while (node.NearestToStart != null)
            {
                list.Add(node);
                LengthOfShortestPath += node.Connections.Single(x => x.ConnectedToNode == node.NearestToStart).Length;
                CostOfShortestPath += node.Connections.Single(x => x.ConnectedToNode == node.NearestToStart).Cost;
                node = node.NearestToStart;
            }
            list.Add(node);
        }

        public double GetHeuristics(Node firstNode, Node secondNode)
        {
            return Math.Sqrt(Math.Pow(firstNode.Point.X - secondNode.Point.X, 2) + Math.Pow(firstNode.Point.Y - secondNode.Point.Y, 2));
            //return Math.Abs(firstNode.Point.X - secondNode.Point.X) + Math.Abs(firstNode.Point.Y - secondNode.Point.Y);
        }
    }
}
