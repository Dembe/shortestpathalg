﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Map;

namespace SearchSystem
{
    public class DijkstraSearch : ISearchAlgorithm
    {
        public int CountOfNodeVisit { get; set; }
        public double LengthOfShortestPath { get; set; }
        public double CostOfShortestPath { get; set; }
        public Node Start { get; set; }
        public Node End { get; set; }
        public Graph Graph { get; set; }
        public DijkstraSearch(Graph graph)
        {
            Graph = graph;
            End = graph.EndNode;
            Start = graph.StartNode;
        }

        public void GetShortestPath(Stopwatch stopWatch)
        {
            CountOfNodeVisit = 0;
            Start.MinCostToStart = 0;

            stopWatch.Start();

            var priorityQueue = new List<Node>();
            priorityQueue.Add(Start);

            do
            {
                CountOfNodeVisit++;
                priorityQueue = priorityQueue.OrderBy(x => x.MinCostToStart.Value).ToList();
                var node = priorityQueue.First();

                priorityQueue.Remove(node);

                foreach (var conn in node.Connections.OrderBy(x => x.Cost))
                {
                    var child = conn.ConnectedToNode;

                    if (child.Visited)
                        continue;
                    if (child.MinCostToStart == null || node.MinCostToStart + conn.Cost < child.MinCostToStart)
                    {
                        child.MinCostToStart = node.MinCostToStart + conn.Cost;
                        child.NearestToStart = node;

                        if (!priorityQueue.Contains(child))
                        {
                            priorityQueue.Add(child);
                        }
                    }
                }

                node.Visited = true;

                if (node == End) break;
            } while (priorityQueue.Any());

            Graph.ShortestPath.Add(End);
            BuiltRecursiveShortestPath(Graph.ShortestPath, End);
            Graph.ShortestPath.Reverse();

            stopWatch.Stop();
        }

        public void BuiltRecursiveShortestPath(List<Node> list, Node node)
        {
            if(node.NearestToStart == null) return;

            list.Add(node.NearestToStart);
            LengthOfShortestPath += node.Connections.Single(x => x.ConnectedToNode == node.NearestToStart).Length;
            CostOfShortestPath += node.Connections.Single(x => x.ConnectedToNode == node.NearestToStart).Cost;
            BuiltRecursiveShortestPath(list, node.NearestToStart);
        }
    }
}
