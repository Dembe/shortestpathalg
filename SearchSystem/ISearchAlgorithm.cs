﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Map;

namespace SearchSystem
{
    public interface ISearchAlgorithm
    {
        int CountOfNodeVisit { get; set; }
        double LengthOfShortestPath { get; set; }
        double CostOfShortestPath { get; set; }
        Node Start { get; set; }
        Node End { get; set; }
        void GetShortestPath(Stopwatch stopWatch);
        void BuiltRecursiveShortestPath(List<Node> list, Node node);
    }
}
