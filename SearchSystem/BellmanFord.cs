﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Map;

namespace SearchSystem
{
    public class BellmanFord : ISearchAlgorithm
    {
        public int CountOfNodeVisit { get; set; }
        public double LengthOfShortestPath { get; set; }
        public double CostOfShortestPath { get; set; }
        public Node Start { get; set; }
        public Node End { get; set; }
        public Graph Graph { get; set; }

        public BellmanFord(Graph graph)
        {
            Graph = graph;
            End = graph.EndNode;
            Start = graph.StartNode;
        }

        public void GetShortestPath(Stopwatch stopWatch)
        {
            CountOfNodeVisit = 0;

            var distanceFromStart = new Dictionary<Node, double>();
            foreach (var node in Graph.Nodes)
            {
                distanceFromStart.Add(node, double.MaxValue);
            }

            distanceFromStart[Start] = 0;

            stopWatch.Start();

            for (int i = 1; i < Graph.Nodes.Count; i++)
            {
                foreach (var node in Graph.Nodes)
                {
                    var u = node;
                    u.Visited = true;
                    foreach (var conn in node.Connections)
                    {
                        var v = conn.ConnectedToNode;
                        var w = conn.Cost;

                        if (distanceFromStart[u] + w < distanceFromStart[v])
                        {
                            distanceFromStart[v] = distanceFromStart[u] + w;
                            v.NearestToStart = u;
                        }
                    }
                }
            }

            foreach (var node in Graph.Nodes)
            {
                foreach (var conn in node.Connections)
                {
                    var v = conn.ConnectedToNode;
                    var w = conn.Cost;
                    CountOfNodeVisit++;
                    if (distanceFromStart[node] + w < distanceFromStart[v])
                        throw new InvalidDataException("There is negative-weight cycle in graph!");
                }
            }

            Graph.ShortestPath.Add(End);

            BuiltRecursiveShortestPath(Graph.ShortestPath, End);
            Graph.ShortestPath.Reverse();

            stopWatch.Stop();
        }

        public void BuiltRecursiveShortestPath(List<Node> list, Node node)
        {
            if (node.NearestToStart == null) return;

            list.Add(node.NearestToStart);
            LengthOfShortestPath += node.Connections.Single(x => x.ConnectedToNode == node.NearestToStart).Length;
            CostOfShortestPath += node.Connections.Single(x => x.ConnectedToNode == node.NearestToStart).Cost;
            BuiltRecursiveShortestPath(list, node.NearestToStart);
        }
    }
}
