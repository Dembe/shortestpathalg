﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Map;

namespace SearchSystem
{
    public class DStar : ISearchAlgorithm
    {
        public int CountOfNodeVisit { get; set; }
        public double LengthOfShortestPath { get; set; }
        public double CostOfShortestPath { get; set; }
        public Node Start { get; set; }
        public Node End { get; set; }
        public Graph Graph { get; set; }

        private List<int> OPEN_LIST = new List<int>();


        public DStar(Graph graph)
        {
            Graph = graph;
            End = graph.EndNode;
            Start = graph.StartNode;
        }

        private void Init()
        {
            foreach (var node in Graph.Nodes)
            {
                node.Tag = "NEW";
                node.MinCostToStart = 0;
                node.parentIndex = -1;
            }

            End.MinCostToStart = 0;
            Start.MinCostToStart = 0;
        }

        public void GetShortestPath(Stopwatch stopWatch)
        {
            Init();
            CountOfNodeVisit = 0;
            int startIndex = Graph.Nodes.IndexOf(Start);
            int endIndex = Graph.Nodes.IndexOf(End);
            stopWatch.Start();
            Insert(endIndex, (double) Graph.Nodes[endIndex].MinCostToStart);

            double proc = 0;

            while (proc > -1)
            {
                CountOfNodeVisit++;
                proc = ProcessGo();
            }

            BuiltRecursiveShortestPath(Graph.ShortestPath, Start);
            stopWatch.Stop();
        }

        public void BuiltRecursiveShortestPath(List<Node> list, Node node)
        {
            while (node.NearestToStart != null)
            {
                list.Add(node);
                LengthOfShortestPath += node.Connections.Single(x => x.ConnectedToNode == node.NearestToStart).Length;
                CostOfShortestPath += node.Connections.Single(x => x.ConnectedToNode == node.NearestToStart).Cost;
                node = node.NearestToStart;
            }

            list.Add(node);
        }

        private void Insert(int nodeID, double hValue)
        {
            double hNew = -1;
            if (Graph.Nodes[nodeID].Tag == "NEW")
            {
                Graph.Nodes[nodeID].Tag = "OPEN";
                hNew = hValue;
                Graph.Nodes[nodeID].hMin = hNew;
            }

            if (Graph.Nodes[nodeID].Tag == "OPEN")
            {
                hNew = Math.Min(Graph.Nodes[nodeID].hMin, hValue);
                Graph.Nodes[nodeID].hMin = hNew;
            }
            else
            {
                hNew = Math.Min((double) Graph.Nodes[nodeID].MinCostToStart, hValue);
                Graph.Nodes[nodeID].hMin = hNew;
                Graph.Nodes[nodeID].MinCostToStart = hValue;
                Graph.Nodes[nodeID].Tag = "OPEN";
            }

            Graph.Nodes[nodeID].MinCostToStart = hValue;

            if (OPEN_LIST.Count > 0)
            {
                int sum = 0;

                for (int i = 0; i < OPEN_LIST.Count; i++)
                {
                    var tmpNode = OPEN_LIST[i];

                    if (Graph.Nodes[nodeID].hMin <= Graph.Nodes[tmpNode].hMin)
                    {
                        OPEN_LIST.Insert(i, nodeID);
                        break;
                    }
                    else
                        sum++;
                }

                if (sum == OPEN_LIST.Count)
                    OPEN_LIST.Add(nodeID);
            }
            else
            {
                OPEN_LIST.Add(nodeID);
            }
        }

        private double ProcessGo()
        {
            double oldK = -1;
            int x = GetMin();
            Graph.Nodes[x].Visited = true;
            double hX = (double) Graph.Nodes[x].MinCostToStart;

            if (x == -1)
            {
                return -1;
            }

            oldK = GetHMin();

            Delete(x);

            if (oldK < hX)
            {
                foreach (var connection in Graph.Nodes[x].Connections)
                {
                    var childNode = connection.ConnectedToNode;

                    int indexChild = Graph.Nodes.IndexOf(childNode);

                    double hY = (double) childNode.MinCostToStart;
                    if (hY <= oldK && hX > hY + connection.Cost)
                    {
                        Graph.Nodes[x].parentIndex = indexChild;
                        Graph.Nodes[x].NearestToStart = childNode;
                        hX = hY + connection.Cost;
                        Graph.Nodes[x].MinCostToStart = hX;
                    }

                }
            }

            if (oldK == Graph.Nodes[x].MinCostToStart)
            {
                foreach (var connection in Graph.Nodes[x].Connections)
                {
                    var childNode = connection.ConnectedToNode;

                    int indexChild = Graph.Nodes.IndexOf(childNode);

                    double hY = (double) childNode.MinCostToStart;
                    int parentY = childNode.parentIndex;
                    string tagY = childNode.Tag;
                    if ((tagY == "NEW") || (parentY == x && hY != hX + connection.Cost) ||
                        (parentY != x && hY > hX + connection.Cost))
                    {
                        double newHX;
                        childNode.parentIndex = x;
                        childNode.NearestToStart = Graph.Nodes[x];

                        newHX = hX + connection.Cost;
                        Insert(indexChild, newHX);
                    }
                }
            }
            else
            {
                foreach (var connection in Graph.Nodes[x].Connections)
                {
                    var childNode = connection.ConnectedToNode;

                    int indexChild = Graph.Nodes.IndexOf(childNode);

                    double hY = (double) childNode.MinCostToStart;
                    int parentY = childNode.parentIndex;
                    string tagY = childNode.Tag;

                    if ((tagY == "NEW") || (parentY == x && hY != hX + connection.Cost))
                    {
                        double newHY;
                        childNode.parentIndex = x;
                        childNode.NearestToStart = Graph.Nodes[x];
                        newHY = hX + connection.Cost;
                        Insert(indexChild, newHY);
                    }
                    else
                    {
                        if (indexChild != x && hY > hX + connection.Cost)
                        {
                            Insert(x, hX);
                        }
                        else if (indexChild != x && hX > hY + connection.Cost)
                        {
                            Insert(indexChild, hY);
                        }
                    }
                }
            }

            if (Graph.Nodes[x] == Start) return -1;

            return GetHMin();
        }

        private int GetMin()
        {
            return OPEN_LIST.Count > 0 ? OPEN_LIST[0] : -1;
        }

        private double GetHMin()
        {
            return OPEN_LIST.Count > 0 ? Graph.Nodes[OPEN_LIST[0]].hMin : -1;
        }

        private void Delete(int nodeID)
        {
            OPEN_LIST.Remove(nodeID);
            Graph.Nodes[nodeID].Tag = "CLOSED";
        }

        //private void Rebuilt(int k)
        //{
        //    var node = Graph.Nodes[k];
        //    while (node.NearestToStart != null)
        //    {
        //        Graph.ShortestPath.Add(node);

        //        node = node.NearestToStart;
        //        LengthOfShortestPath += node.Connections.Single(x => x.ConnectedToNode == node.NearestToStart).Length;
        //        CostOfShortestPath += node.Connections.Single(x => x.ConnectedToNode == node.NearestToStart).Cost;
        //    }

        //    Graph.ShortestPath.Add(node);
        //}

        //public void Test()
        //{
        //    Init();

        //    int startIndex = Graph.Nodes.IndexOf(Start);
        //    int endIndex = Graph.Nodes.IndexOf(End);

        //    Insert(endIndex, (double)Graph.Nodes[endIndex].MinCostToStart);


        //    while (OPEN_LIST.Count > 0)
        //    {
        //        var point = OPEN_LIST[0];
        //        Delete(point);
        //        Expand(point);
        //    }
        //}

        //private void Expand(int point)
        //{
        //    var isRise = IsRaise(point);
        //    double cost;
        //    var node = Graph.Nodes[point];

        //    foreach (var nodeConnection in node.Connections)
        //    {
        //        if (isRise)
        //        {
        //            if (nodeConnection.ConnectedToNode.NearestToStart == node)
        //            {
        //                nodeConnection.ConnectedToNode.parentIndex = point;
        //                nodeConnection.ConnectedToNode.NearestToStart = node;
        //                nodeConnection.ConnectedToNode.MinCostToStart = node.MinCostToStart + nodeConnection.Cost;
        //                Insert(Graph.Nodes.IndexOf(nodeConnection.ConnectedToNode), (double)node.MinCostToStart + nodeConnection.Cost);
        //            }
        //            else
        //            {
        //                cost = (double)node.MinCostToStart + nodeConnection.Cost;
        //                if (cost < nodeConnection.ConnectedToNode.MinCostToStart)
        //                {
        //                    node.hMin = (double)node.MinCostToStart;
        //                    Insert(point, node.hMin);
        //                }
        //            }
        //        }
        //        else
        //        {
        //            cost = (double)node.MinCostToStart + nodeConnection.Cost;
        //            if (cost < nodeConnection.ConnectedToNode.MinCostToStart)
        //            {
        //                nodeConnection.ConnectedToNode.parentIndex = point;
        //                nodeConnection.ConnectedToNode.NearestToStart = node;
        //                nodeConnection.ConnectedToNode.MinCostToStart = node.MinCostToStart + nodeConnection.Cost;
        //                Insert(Graph.Nodes.IndexOf(nodeConnection.ConnectedToNode), (double)node.MinCostToStart + nodeConnection.Cost);
        //            }

        //        }
        //    }
        //    if(node == Start)
        //        OPEN_LIST.Clear();
        //}

        //private bool IsRaise(int point)
        //{
        //    double cost;
        //    if (Graph.Nodes[point].MinCostToStart >= Graph.Nodes[point].hMin)
        //    {
        //        foreach (var connection in Graph.Nodes[point].Connections)
        //        {
        //            cost = (double)connection.ConnectedToNode.MinCostToStart + connection.Cost;
        //            if (cost < Graph.Nodes[point].MinCostToStart)
        //            {
        //                Graph.Nodes[point].parentIndex = Graph.Nodes.IndexOf(connection.ConnectedToNode);
        //                Graph.Nodes[point].NearestToStart = connection.ConnectedToNode;
        //                Graph.Nodes[point].MinCostToStart = cost;
        //            }
        //        }
        //    }

        //    return Graph.Nodes[point].MinCostToStart > Graph.Nodes[point].hMin;
        //}
    }
}
